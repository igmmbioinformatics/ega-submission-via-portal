/*
 * -------------------------------------------------
 *  ega-submission-via-portal Nextflow config file
 * -------------------------------------------------
 * Default config options for all environments.
 */

// Global default params, used in configs
params {

  // Workflow flags
  reads = "data/*.fastq.gz"
  ega_user = ""
  ega_password = ""
  outdir = './results'
  samples = 'samples.csv'

  // Boilerplate options
  name = false
  help = false
  hostnames = false
  config_profile_description = false
  config_profile_contact = false
  config_profile_url = false

  // Defaults only, expecting to be overwritten
  max_memory = 128.GB
  max_cpus = 16
  max_time = 240.h

}

process {
  withName: EGA_ENCRYPT {
    cpus = 8
    memory = 12.GB
    time = 2.h

    publishDir = [
        path: { params.outdir },
        mode: 'symlink',
        pattern: '*.{gpg,md5}',
        enabled: params.ega_user == null || params.ega_user == ''
    ]
  }

  withName: EGA_COLLECTRUNCSVS {
    cpus = 1
    memory = 2.GB
    time = 10.m

    publishDir = [
        path: { params.outdir },
        mode: 'copy'
    ]
  }

  withName: EGA_UPLOAD {
    cpus = 1
    memory = 2.GB
    time = 8.h
  }
}

profiles {
    standard { conda.enabled = true }
    stubs { conda.enabled = false }
}

// Export this variable to prevent local Python libraries from conflicting with those in the container
env {
  PYTHONNOUSERSITE = 1
}

// Capture exit codes from upstream processes when piping
process.shell = ['/bin/bash', '-euo', 'pipefail']

// Function to ensure that resource requirements don't go beyond
// a maximum limit
def check_max(obj, type) {
  if (type == 'memory') {
    try {
      if (obj.compareTo(params.max_memory as nextflow.util.MemoryUnit) == 1)
        return params.max_memory as nextflow.util.MemoryUnit
      else
        return obj
    } catch (all) {
      println "   ### ERROR ###   Max memory '${params.max_memory}' is not valid! Using default value: $obj"
      return obj
    }
  } else if (type == 'time') {
    try {
      if (obj.compareTo(params.max_time as nextflow.util.Duration) == 1)
        return params.max_time as nextflow.util.Duration
      else
        return obj
    } catch (all) {
      println "   ### ERROR ###   Max time '${params.max_time}' is not valid! Using default value: $obj"
      return obj
    }
  } else if (type == 'cpus') {
    try {
      return Math.min( obj, params.max_cpus as int )
    } catch (all) {
      println "   ### ERROR ###   Max cpus '${params.max_cpus}' is not valid! Using default value: $obj"
      return obj
    }
  }
}
