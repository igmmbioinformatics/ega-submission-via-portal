"""
Generates fake paired-end fastq files to be used for testing purposes.
"""
import os
import gzip
import random
import argparse

phred_ascii_values = list(range(33, 127))
atgc = ['A', 'T', 'G', 'C']
strands = {1: '+', 2: '-'}

def fastq(name, r, reads, read_length=20):
    with gzip.open(name, 'wt') as f:
        for read in reads:
            f.write(f'@read {read} r{r}\n')
            f.write(''.join(random.choice(atgc) for _ in range(read_length)) + '\n')
            f.write(strands[r] + '\n')
            f.write(''.join(chr(random.choice(phred_ascii_values)) for _ in range(read_length)) + '\n')


def main():
    a = argparse.ArgumentParser()
    a.add_argument('--outdir', '-o', default='assets')
    a.add_argument('--nsamples', '-n', type=int, default=10)
    a.add_argument('--reads_per_sample', '-r', type=int, default=4)
    a.add_argument('--read_length', '-l', type=int, default=20)
    args = a.parse_args()

    samples = ['sample%i' % (i + 1) for i in range(args.nsamples)]
    current_read = 1

    for s in samples:
        for strand in (1, 2):
            filename = os.path.join(args.outdir, '%s_R%i.fastq.gz' % (s, strand))
            reads = range(current_read, current_read + args.reads_per_sample + 1)
            fastq(filename, strand, reads, args.read_length)

        current_read += (args.reads_per_sample + 1)


if __name__ == '__main__':
    main()

