import os
import uuid
import hashlib
import unittest
import ega_metadata

uuid_idx = 0
test_path = os.path.dirname(__file__)
obs_dir = os.path.join(test_path, 'xml')
exp_dir = os.path.join(test_path, 'expected')


def fake_uuid1():
    global uuid_idx
    uuid_idx += 1
    return uuid.UUID(int=uuid_idx)


def md5_file(f):
    m = hashlib.md5()
    with open(f) as h:
        for line in h:
            m.update(line.encode())

    return m.hexdigest()


class TestMetadata(unittest.TestCase):
    def setUp(self):
        os.environ['EGA_UPLOAD_CONFIG'] = os.path.join(test_path, 'ega_upload.yaml')
        self.config = ega_metadata.file_args()

        ega_metadata.xml_dir = 'xml'
        ega_metadata.force = True
        self.original_uuid1 = uuid.uuid1
        uuid.uuid1 = fake_uuid1

        os.makedirs(obs_dir, exist_ok=True)
        for f in os.listdir(obs_dir):
            os.remove(os.path.join(obs_dir, f))

    def test_metadata(self):
        ega_metadata.createsubmission(self.config)
        ega_metadata.createpolicy(self.config)
        ega_metadata.createstudy(self.config)
        ega_metadata.createsamples(self.config)
        ega_metadata.createrunsandexperiments(self.config)
        ega_metadata.createdataset(self.config)

        for f in os.listdir(obs_dir):
            obs_md5 = md5_file(os.path.join(obs_dir, f))
            exp_md5 = md5_file(os.path.join(exp_dir, f))
            self.assertEqual(exp_md5, obs_md5, msg='MD5 mismatch for file ' + f)

    def tearDown(self):
        uuid.uuid1 = self.original_uuid1


if __name__ == '__main__':
    unittest.main()
