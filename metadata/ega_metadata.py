import os
import re
import sys
import math
import yaml
import uuid
import jinja2
import pandas
import logging
import argparse
import subprocess
import xml.dom.minidom

multi_newline = re.compile(r'\n{2,}')

options = {
    # universal options - root parser has prog=__name__
    __name__: {
        'debug': {'help': 'Set the logging level to DEBUG', 'action': 'store_true', 'default': False, 'alias': '-d'},
        'force': {'help': 'Always overwrite output XMLs', 'action': 'store_true', 'default': False, 'alias': '-f'},
        'output_dir': {'help': 'Directory to write output XMLs', 'alias': '-o', 'default': 'xml'},
        'center_name': {'help': 'Value to add to the "center_name" attribute in study.xml and submission.xml. This value will depend on your EGA box.'},
        'submission_contacts': {'help': "Colon-separated name and email to add to the submission for notifications, e.g. 'Murray Wham:murray.wham@ed.ac.uk'. Can be specified multiple times.", 'nargs': '+'}
    },
    'createdac': {
        'dac_alias': {'help': 'Alias for the DAC object to create'},
        'dac_title': {'help': 'Short text to show in searches and displays'},
        'dac_contacts': {'help': "Colon-separated name, email and organisation to add to the dac, e.g. 'Murray Wham:murray.wham@ed.ac.uk:IGC'. Can be specified multiple times.", 'nargs': '+'},
        'dac_attributes': {'help': "Colon-separated tag, value and unit of extra attributes to list with the DAC. Can be specified multiple times.", 'nargs': '+'}
    },
    'createpolicy': {
        'policy_alias': {'help': 'Alias for the policy object to create'},
        'policy_title': {'help': 'Policy title'},
        'dac_accession': {'help': 'EGA accession number for the DAC object to link this policy to'},
        'policy_text': {'help': 'Policy text. This or policy_file_url is required.'},
        'policy_file_url': {'help': 'URL of the policy document. This or policy_text is required'},
        'policy_links': {'help': "Semicolon-separated description and URL of policy materials to link to, e.g. 'IGC Data Access Agreement;https://www.ed.ac.uk/some-policy'. Can be specified multiple times.", 'nargs': '+'},
        'policy_attributes': {'help': "Colon-separated tag, value and unit of extra attributes to list with the policy. Can be specified multiple times.", 'nargs': '+'}
    },
    'createstudy': {
        'study_alias': {'help': 'Unique ID for the study. Used to refer to the study during the submission process and is supposed to be globally unique within the submission account, but is not shared with anyone.'},
        'study_type': {'help': 'Study type accepted by EGA, e.g. Whole Genome Sequencing', 'choices': ('Whole Genome Sequencing', 'Metagenomics', 'Transcriptome Analysis', 'Resequencing', 'Epigenetics', 'Synthetic Genomics', 'Forensic or Paleo-genomics', 'Gene Regulation Study', 'Cancer Genomics', 'Population Genomics', 'RNASeq', 'Exome Sequencing', 'Pooled Clone Sequencing', 'Transcriptome Sequencing', 'Other (Study type not listed)')},
        'study_title': {'help': 'Study title'},
        'study_abstract': {'help': 'Abstract for this study'},
        'study_attributes': {'help': "CSV or Excel table with two columns ('tag' and 'value'), describing study attributes to add to study.xml"}
    },
    'createsamples': {
        'samples': {'help': "CSV or Excel table with at least 8 columns ('id', 'title', 'scientific_name', 'common_name', 'description', 'sex', 'phenotype'), describing samples to add. Any additional columns will be included as sample attributes"},
    },
    'createrunsandexperiments': {
        'batch_size': {'help': 'Set this to split paired-end fastqs into smaller batches. This may be required if uploading large datasets - ega-box accounts should not exceed 8Tb and must not exceed 10Tb.', 'default': None},
        'runs': {'help': "Path to CSV file linking samples to files. Must have the columns 'Sample alias', 'First Fastq File', 'First Checksum', 'First Unencrypted checksum', 'Second Fastq File', 'Second Checksum', 'Second Unencrypted checksum'", 'default': 'runs.csv'},
        'experiment_title': {'help': 'Title to apply to all created experiment objects'},
        'library_strategy': {'help': 'Value to apply to LIBRARY_STRATEGY, e.g. WGS'},
        'library_source': {'help': 'Value to apply to LIBRARY_SOURCE, e.g. GENOMIC'},
        'library_selection': {'help': 'Value to apply to LIBRARY_SELECTION', 'default': 'other'},
        'library_nominal_length': {'help': 'Library nominal length, e.g. 450'},
        'library_protocol': {'help': 'Description of the library preparation process'},
        'platform_type': {'help': 'Platform type, e.g. illumina'},
        'platform_instrument': {'help': 'Instrument type, e.g. HiSeq X Ten'},
        'study_alias': {'help': 'Unique ID for the study. Used to refer to the study during the submission process and is supposed to be globally unique within the submission account, but is not shared with anyone.'},
        'file_box_base': {'help': 'Base file path to apply to uploaded files when passing to `filename`, e.g. if files are uploaded to a folder structure on the ega-box FTP server', 'default': ''},
        'sample_xml': {'help': 'Path to the xml file containing sample objects and uuids'}
    },
    'createdataset': {
        'run_receipt': {'help': 'Receipt file containing Runs to include in this dataset. Can be specified multiple times, e.g. if the upload was done in batches.', 'nargs': '+'},
        'policy_receipt': {'help': 'Receipt file containing a Policy to use in this dataset.'},
        'dataset_title': {'help': 'Dataset title'},
        'dataset_type': {'help': 'Dataset type accepted by EGA, e.g. Whole genome sequencing', 'choices': ('Whole genome sequencing', 'Exome sequencing', 'Genotyping by array', 'Transcriptome profiling by high-throughput sequencing', 'Transcriptome profiling by array', 'Amplicon sequencing', 'Methylation binding domain sequencing', 'Methylation profiling by high-throughput sequencing', 'Phenotype information', 'Study summary information', 'Genomic variant calling', 'Chromatin accessibility profiling by high-throughput sequencing', 'Histone modification profiling by high-throughput sequencing', 'Chip-Seq')}
    },
    'submit': {
        'xml': {'help': 'XMLs to submit. Can be specified multiple times', 'nargs': '+', 'alias': '-x'},
        'url': {'help': 'URL of the EGA metadata service to submit to'},
        'ega_box': {'help': 'ega-box account to use for upload'},
        'egapass': {'help': 'egapass file containing password for authenticating to metadata service'},
        'receipts_dir': {'help': 'Folder to write receipts to', 'default': 'receipts'},
        'dry_run': {'help': 'Submit with the action VALIDATE instead of ADD', 'action': 'store_true'}
    }
}

env = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__), 'templates')),
    autoescape=jinja2.select_autoescape()
)

formatter = logging.Formatter('[%(asctime)s][%(name)s][%(levelname)s] %(message)s')
handler = logging.StreamHandler(sys.stdout)
handler.setFormatter(formatter)
handler.setLevel(logging.INFO)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

xml_dir = ''
force = False


def cmd_args(argv=None):
    a = argparse.ArgumentParser(__name__)
    subparsers = a.add_subparsers()
    for n in options:
        if n == __name__:
            p = a
        else:
            p = subparsers.add_parser(n)

        p.set_defaults(entry=p.prog)
        for k, v in options[p.prog.replace(__name__ + ' ', '')].items():
            alias = v.get('alias')  # e.g. -s for --study_alias
            names = ['--' + k]
            if alias:
                v.pop('alias')
                names.append(alias)

            p.add_argument(*names, **v)

    return a.parse_args(argv)


def file_args():
    for f in (os.getenv('EGA_UPLOAD_CONFIG'), os.path.join(os.getcwd(), 'ega_upload.yaml')):
        if f and os.path.isfile(f):
            with open(f) as h:
                return yaml.safe_load(h)
    return {}


def _merge_file_and_cmd_args(file_config, args):
    config = dict()
    for k in {__name__, args.entry.replace(__name__ + ' ', '')}:
        for k2, v in options[k].items():
            # first populate config from command line, but only when different from the default
            cmd_v = args.__dict__.get(k2)
            if cmd_v is not None and cmd_v != v.get('default'):
                config[k2] = cmd_v

            # next with file config
            elif k2 in file_config:
                config[k2] = file_config[k2]

            # finally with declared defaults
            elif 'default' in v:
                config[k2] = v['default']

    return config


def configure(argv=None):
    args = cmd_args(argv)
    file_conf = file_args()
    config = _merge_file_and_cmd_args(file_conf, args)

    if config['debug']:
        handler.setLevel(logging.DEBUG)
        logger.setLevel(logging.DEBUG)

    logger.info('Output dir: %s', config['output_dir'])
    global xml_dir
    xml_dir = config['output_dir']
    os.makedirs(xml_dir, exist_ok=True)

    global force
    if config['force']:
        force = True

    return args.entry, config


def main(argv=None):
    logger.addHandler(handler)
    entry, config = configure(argv)

    # xml submission
    if entry.endswith('submit'):
        return submit(config)

    # xml creation
    # we'll always need this
    createsubmission(config)

    if entry.endswith('createdac'):
        createdac(config)
    elif entry.endswith('createpolicy'):
        createpolicy(config)
    elif entry.endswith('createstudy'):
        createstudy(config)
    elif entry.endswith('createsamples'):
        createsamples(config)
    elif entry.endswith('createrunsandexperiments'):
        createrunsandexperiments(config)
    elif entry.endswith('createdataset'):
        createdataset(config)


def submit(config):
    # trim path/to/experiment-1.xml to experiment-1
    xml_basenames = [os.path.splitext(os.path.basename(x))[0] for x in config['xml']]

    # check if we've done this before or want to overwrite the receipt
    os.makedirs(config['receipts_dir'], exist_ok=True)
    receipt_file = os.path.join(config['receipts_dir'], '_'.join(sorted(xml_basenames)) + '.xml')
    if os.path.isfile(receipt_file) and not force:
        logger.info('%s already exists', receipt_file)
        return

    # check for collisions of e.g. folder1/study.xml and folder2/study.xml
    for x in set(xml_basenames):
        nfiles = xml_basenames.count(x)
        if nfiles > 1:
            raise ValueError('Naming collision: %i files found called %s' % (nfiles, x))

    dry_run = config.get('dry_run', False)
    action = 'VALIDATE' if dry_run else 'ADD'
    object_types = {o: o.upper() for o in ('dac', 'policy', 'study', 'sample', 'run', 'experiment', 'dataset')}

    submission_xml = os.path.join(xml_dir, 'submission.xml')
    if not os.path.isfile(submission_xml):
        raise FileNotFoundError('No submission.xml found')

    cmd = 'curl -u "{ega_box}:$(cat {egapass})" {url} -F ACTION={action} -F SUBMISSION=@{submission_xml} '.format(
        ega_box=config['ega_box'], egapass=config['egapass'], url=config['url'], action=action, submission_xml=submission_xml
    )

    for xml in config['xml']:
        file_base = os.path.splitext(os.path.basename(xml))[0].split('-')[0]
        object_type = object_types[file_base]
        cmd += '-F %s=@%s ' % (object_type, xml)

    logger.info('Submitting xmls %s', ', '.join(config['xml']))

    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    out, err = p.communicate()
    exit_status = p.wait()
    if exit_status != 0:
        logger.error('Submission failed with exit status $i', exit_status)

    for line in out.decode().split('\n'):
        logger.info(line)

    for line in err.decode().split('\n'):
        logger.error(line)

    if not dry_run:
        logger.info('Saving to %s', receipt_file)
        with open(receipt_file, 'w') as f:
            f.write(out.decode())


def createsubmission(config):
    write_xml(
        'submission.xml',
        config=config,
        submission_contacts=[c.split(':') for c in config.get('submission_contacts', ())]
    )


def createdac(config):
    dac_attributes = []
    for attr in config.get('dac_attributes', ()):
        split_attr = attr.split(':')
        if len(split_attr) == 2:  # no unit
            split_attr.append(None)

        dac_attributes.append(split_attr)

    write_xml(
        'dac.xml',
        config=config,
        dac_contacts=[c.split(':') for c in config.get('dac_contacts', ())],
        dac_attributes=dac_attributes
    )


def createpolicy(config):
    if not config['policy_text'] and not config['policy_file_url']:
        raise KeyError('policy_text or policy_file_url is required')

    policy_attributes = []
    for attr in config.get('policy_attributes', ()):
        split_attr = attr.split(':')
        if len(split_attr) == 2:  # no unit
            split_attr.append(None)

        policy_attributes.append(split_attr)

    write_xml(
        'policy.xml',
        config=config,
        policy_links=[l.split(':') for l in config.get('policy_links', ())],
        policy_attributes=policy_attributes
    )


def createstudy(config):
    cfg = config.get('study_attributes')
    if cfg:
        df = read_table(config['study_attributes'])
        if list(df['tag']) != list(set(df['tag'])):
            raise ValueError('Duplicate tags found in %s' % config['study_attributes'])

        study_attributes = {row.tag: row.value for row in df.itertuples()}
    else:
        study_attributes = {}

    write_xml('study.xml', config=config, study_attributes=study_attributes)


def createsamples(config):
    idgen = IDGenerator(xml_dir)
    df = read_table(config['samples'])
    mandatory_columns = ('id', 'taxon_id', 'scientific_name', 'common_name', 'description', 'sex', 'phenotype')
    extra_cols = tuple(c for c in df.columns if c not in mandatory_columns)
    if tuple(df.columns[:7]) != mandatory_columns:
        raise ValueError('First 7 columns of %s must be %s' % (config['samples'], mandatory_columns))

    samples = []
    for i, s in df.iterrows():
        new_sample = dict()
        for k in mandatory_columns:
            v = s[k]
            if pandas.notna(v):
                new_sample[k] = v

        extra_attributes = {c: s[c] for c in extra_cols if pandas.notna(s[c])}
        new_sample['alias'] = idgen.new()
        new_sample['attributes'] = extra_attributes
        samples.append(new_sample)

    write_xml('sample.xml', config=config, samples=samples)


def createrunsandexperiments(config):
    idgen = IDGenerator(xml_dir)
    df = read_table(config['runs'])
    df['experiment_id'] = [idgen.new() for _ in range(len(df))]
    df['run_id'] = [idgen.new() for _ in range(len(df))]
    df['r1'] = df['First Fastq File'].map(lambda f: os.path.join(config['file_box_base'], f))
    df['r2'] = df['Second Fastq File'].map(lambda f: os.path.join(config['file_box_base'], f))
    df['file_type_r1'] = df['First Fastq File'].map(get_file_type)
    df['file_type_r2'] = df['Second Fastq File'].map(get_file_type)

    sample_map = {}
    dom = xml.dom.minidom.parse(config['sample_xml'])
    for s in dom.getElementsByTagName('SAMPLE'):
        sample_uuid = s.attributes['alias'].value
        sample_name = s.getElementsByTagName('TITLE')[0].childNodes[0].data
        sample_map[sample_name] = sample_uuid

    nsamples = len(df)
    batch_size = config['batch_size'] or nsamples
    slices = batches(nsamples, batch_size)
    logger.info('Splitting %i samples into %i batches of size %i', nsamples, len(slices), batch_size)

    for i, (j, k) in enumerate(slices):
        batch = [line for i, line in df[j:k].iterrows()]
        write_xml('experiment.xml', 'experiment-%i.xml' % (i + 1), id_map=batch, config=config, sample_map=sample_map)
        write_xml('run.xml', 'run-%i.xml' % (i + 1), id_map=batch, config=config)


def createdataset(config):
    run_accessions = set()
    for r in config['run_receipt']:
        dom = xml.dom.minidom.parse(r)
        for r in dom.getElementsByTagName('RUN'):
            run_accessions.add(r.attributes['accession'].value)

    dom = xml.dom.minidom.parse(config['policy_receipt'])
    policy_accession = dom.getElementsByTagName('POLICY')[0].attributes['accession'].value

    idgen = IDGenerator(xml_dir)
    write_xml(
        'dataset.xml',
        config=config,
        dataset_alias=idgen.new(),
        policy_accession=policy_accession,
        runs=sorted(run_accessions),
        analyses=()
    )


def read_table(f: str) -> pandas.DataFrame:
    if f.endswith('.csv'):
        return pandas.read_csv(f)
    elif f.endswith('xlsx'):
        return pandas.read_excel(f, engine='openpyxl')
    else:
        raise NameError('Unrecognised file format for %s - must be csv or xlsx' % f)


def render_xml(template, **kwargs):
    return multi_newline.sub('\n', env.get_template(template).render(**kwargs))


def write_xml(template_name, xml_name_out=None, **kwargs):
    xml_name_out = xml_name_out or template_name
    path = os.path.join(xml_dir, xml_name_out)
    if os.path.isfile(path) and not force:
        logger.info('%s already exists', xml_name_out)
        return

    content = render_xml(template_name, **kwargs)
    with open(path, 'w') as f:
        f.write(content)

    logger.info('Written %s', xml_name_out)
    logger.debug(content)


class IDGenerator:
    def __init__(self, xml_dir=None):
        if xml_dir:
            self.existing_ids = self.scrape_xml_dir(xml_dir)
        else:
            self.existing_ids = set()

    def new(self):
        candidate = None
        for _ in range(3):
            candidate = str(uuid.uuid1())

            if candidate in self.existing_ids:
                logger.warning('Attempted to generate an ID %s that already existed - retrying', candidate)
            else:
                self.existing_ids.add(candidate)
                return candidate

        raise ValueError('Failed to generate an experiment ID. Last tried: %s' % candidate)

    @staticmethod
    def scrape_xml_dir(xml_dir):
        ids = set()
        tags = ('DAC', 'POLICY', 'EXPERIMENT', 'RUN', 'STUDY', 'SAMPLE')
        for root, dirs, files in os.walk(xml_dir):
            for f in files:
                if not f.endswith('.xml'):
                    continue

                dom = xml.dom.minidom.parse(os.path.join(root, f))
                for t in tags:
                    elements = dom.getElementsByTagName(t)
                    for e in elements:
                        if 'alias' in e.attributes:
                            ids.add(e.attributes.get('alias').value)

        return ids


def get_file_type(filename):
    if filename.endswith('fq.gz') or filename.endswith('fastq.gz'):
        return 'fastq'

    raise NameError('Could not determine file type for file %s' % filename)


def batches(nitems, batch_size):
    """
    Split a number of items (`nitems`) into slices of `batch_size` size. The final
    slice may be smaller due to the remainder. E.g, splitting 99 fastq pairs into
    batches of 25 -> [25, 25, 25, 24] -> [(0, 25), (25, 50), (50, 75), (75, 99)].
    """
    nfull_batches = nitems // batch_size
    _batches = [
        (i * batch_size, (i * batch_size) + batch_size)
        for i in range(nfull_batches)
    ]
    remainder = nitems % batch_size
    if remainder:
        last_batch = (nitems - remainder, nitems)
        _batches.append(last_batch)

    return _batches


if __name__ == '__main__':
    main()

