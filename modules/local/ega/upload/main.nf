/*
 * STEP 3 - Upload output via Aspera to EGA box
 */
process EGA_UPLOAD {
    tag "${sample}"
    conda "${moduleDir}/environment.yml"

    when: params.ega_user

    input:
    tuple val(sample), path(files)
    val(egapass)  // not a path to avoid excessive linking and accidental copying of pass file if stageInMode 'copy' is used

    script:
    """
    ls ${egapass[0]}
    export ASPERA_SCP_PASS=\$(cat ${egapass[0]})
    ascp -T -P 33001 -O 33001 -l 300M -QT -L- -k 1 ${sample}* ${params.ega_user}@fasp.ega.ebi.ac.uk:/.
    """

    stub:
    """
    echo 'Would run:'
    echo 'export ASPERA_SCP_PASS=\$(cat ${egapass[0]})'
    echo 'ascp -T -P 33001 -O 33001 -l 300M -QT -L- -k 1 ${sample}* ${params.ega_user}@fasp.ega.ebi.ac.uk:/.'
    """
}
