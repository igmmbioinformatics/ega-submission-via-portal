/*
 * STEP 2 - Collect the CSV output for runs
 */

process EGA_COLLECTRUNCSVS {
    input:
    path(files)

    output:
    path("runs.csv")

    script:
    """
    echo \"Sample alias\",\"First Fastq File\",\"First Checksum\",\"First Unencrypted checksum\",\"Second Fastq File\",\"Second Checksum\",\"Second Unencrypted checksum\" > runs.csv
    cat ${files} | sort -V >> runs.csv
    """
}

