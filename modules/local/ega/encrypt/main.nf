/*
 * STEP 1 - Encrypt the FASTQ files. Generate a line of CSV output for runs, if not uploading to EGA, move the
 * encrypted files and md5 checksums to the output directory.
 */
process EGA_ENCRYPT {
    tag "$sample"
    conda "${moduleDir}/environment.yml"

    input:
    tuple val(sample), path(reads)

    output:
    tuple val(sample), path('*.csv'), emit: csv
    tuple val(sample), path('*.*'), emit: all

    script:
    """
    java -Xmx8g -jar ${moduleDir}/resources/ega-cryptor-2.0.0.jar -i ${reads[0]} -t 8 -o .
    java -Xmx8g -jar ${moduleDir}/resources/ega-cryptor-2.0.0.jar -i ${reads[1]} -t 8 -o .

    echo "${sample},${sample}_R1.fastq.gz,`cat ${sample}_R1.fastq.gz.gpg.md5`,`cat ${sample}_R1.fastq.gz.md5`,${sample}_R2.fastq.gz,`cat ${sample}_R2.fastq.gz.gpg.md5`,`cat ${sample}_R2.fastq.gz.md5`" > ${sample}.csv
    """

    stub:
    """
    for f in ${reads}
    do
        touch \${f}.{md5,gpg,gpg.md5}
    done

    echo "${sample},${sample}_R1.fastq.gz,`cat ${sample}_R1.fastq.gz.gpg.md5`,`cat ${sample}_R1.fastq.gz.md5`,${sample}_R2.fastq.gz,`cat ${sample}_R2.fastq.gz.gpg.md5`,`cat ${sample}_R2.fastq.gz.md5`" > ${sample}.csv
    """
}

